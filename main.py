import requests
import pymongo
import sys
import json
keys = ['income_statement', 'balance_sheet', 'other_statement']
ratios = {
  'GPMargin':                   {'key': '1_gpm', 'name': 'Gross Profit Margin (%)'},
  'netPMargin':                 {'key': '2_net', 'name': 'Net Profit Margin (%)'},
  'profitAfterTaxToNetPremium': {'key': '3_net', 'name': 'Net Profit Margin (%)'},
  'priceEarningRatio':          {'key': '4_per', 'name': 'PE Ratio'},
  'eps':                        {'key': '5_eps', 'name': 'EPS'},
  'earningPerShare':            {'key': '5_eps', 'name': 'EPS'},
}
financials = [
  {'name': 'Sales',                 'key': '1_sales',      'keyword': 'sales'},
  {'name': 'Mark-up Earned',        'key': '2_markup',     'keyword': 'mark-up_earned'},
  {'name': 'Net Premium',           'key': '3_netPremium', 'keyword': 'net_premium'},
  {'name': 'Total Income',          'key': '4_income',     'keyword': 'total_income'},
  {'name': 'Profit after Taxation', 'key': '5_pat',        'keyword': 'profit_after_taxation'},
  {'name': 'EPS',                   'key': '6_eps',        'keyword': 'eps'}
]
qualitative = [
  {'key': 'secretary', 'keyword': 'company_secretary'},
  {'key': 'auditor',   'keyword': 'auditors'},
  {'key': 'address',   'keyword': 'registered_office'},
  {'key': 'website',   'keyword': 'company_website'},
  {'key': 'ceo',       'keyword': 'ceo'},
  {'key': 'chairman',  'keyword': 'chairman'},
  {'key': 'registrar', 'keyword': 'share_registrar'},
]


def get_symbols():
    data = {
        'item': 'market',
        'timeout': 10000,
    }
    r = requests.post(url="https://market.capitalstake.com/rq", data=data)
    data = r.json()
    return data['data']['eq']


def api_call(symbol, param):

    url = 'https://www.capitalstake.com/api/v2/'+param+'/' + symbol

    hdr = {
        'Content-type': 'application/json',
        'Accept': 'application/json'
    }

    dt = {
        'X-Authorization': '937d85b573502cfa4933c1cad6a94713f3ee011b'
    }
    r = requests.post(url, headers=hdr, data=json.dumps(dt))
    if r.status_code == 200:
        print('OK!')
        data = r.json()
        if data:
            data = data[0]
            return data

    return None


def populate_name(ratio):
    names = {}
    for key in ratio:
        nested_dict = ratio[key]
        m_key = nested_dict['key']
        if m_key not in names.keys():
            names[m_key] = nested_dict['name']
    return names


def remove_keys(rows):
    updated = {}
    red_keys = ['3_net', '4_per', '5_eps']
    for key in red_keys:
        try:
            del rows[key]
        except KeyError:
            print("Key" + key + "not found")

    for key in rows:
        if not bool(rows[key]):
            continue
        updated[key] = rows[key]

    return updated


def calculate_ratios(rows):
    print("Calculating eps growth")
    rows['8_epsg'] = {}
    rows['9_peg'] = {}
    if '5_eps' in rows:
        for key in rows['5_eps']:
            if rows['5_eps'][key] is None:
                continue

            curr = rows['5_eps'][key]
            if str(int(key)-1) in (rows['5_eps']):
                past = rows['5_eps'][str(int(key)-1)]
            else:
                continue

            epsg = ((float(curr)/float(past))-1) * 100 if float(past) != 0.00 else None

            rows['8_epsg'][key] = epsg
            if rows['4_per'][key] is None:
                continue
            peg = float(rows['4_per'][key])/float(epsg) if epsg is not None and epsg != 0 else None
            rows['9_peg'][key] = peg

    return rows


def populate_rows(ratio, fundamentals):
    print("populate rows")
    rows = {}
    t_key = {}
    for key in ratio:
        for f in fundamentals:
            if f['year'] == 'TTM':
                continue
            if key in f:
                year = f['year']
                value = f[key]
                t_key[str(year)] = value
        rows[ratio[key]['key']] = t_key.copy()
        t_key.clear()

    rows = calculate_ratios(rows)
    rows = remove_keys(rows)

    return rows


def match_q_key(f_key):
    for qual in qualitative:
        if qual['keyword'] == f_key:
            return True, qual

    return False, None


def normalize_keys(financial):
    financial = financial.lower()
    financial = financial.replace(" ", "_")
    return financial


def match_key(f_key):
    for financial in financials:
        if financial['keyword'] == f_key:
            return True, financial

    return False, None


def populate_years(data_annual, q=False):

    if 'year' in data_annual:
        yr = data_annual['year']
    else:
        yr = data_annual['Year'] if 'Year' in data_annual else data_annual['YEAR']

    del yr[0]

    yr = yr[:5] if q else yr[:4]

    return yr


def get_quarter(data_quarter):

    q = data_quarter['Quarter'] if 'Quarter' in data_quarter else data_quarter['quarter']
    del q[0]
    return q[:5]


def get_row(financial, years):
    row = {}
    del financial[0]
    financial = financial[:5]
    for year, val in zip(years, financial):
        if 'Q4' in str(year):
            continue
        row[str(year)] = val

    return row


def populate_annuals(data_annual):
    years = []
    names = {}
    rows = {}
    for key in keys:
        if data_annual[key] is None:
            continue
        for financial in data_annual[key]:
            f_key = normalize_keys(financial)
            if not years:
                years = populate_years(data_annual[key])
            check, f = match_key(f_key)
            if check:
                rows[f['key']] = get_row(data_annual[key][financial], years)
                names[f['key']] = f['keyword']

    annual = {"name": names, 'years': years, 'rows': rows} if names or years or rows else None

    return annual


def remove_quarter(qtr_yr):
    u_qtr = []
    for qtr in qtr_yr:
        if "Q4" in qtr:
            continue
        u_qtr.append(qtr)
    return u_qtr


def populate_quarter(data_quarter):
    names = {}
    rows = {}
    years = []

    for key in keys:
        for financial in data_quarter[key]:
            f_key = normalize_keys(financial)
            if not years:
                years = populate_years(data_quarter[key], True)
                quarter = get_quarter(data_quarter[key])
                qtr_yr = [str(j)+" "+str(i) for i, j in zip(years, quarter)]
            check, f = match_key(f_key)
            if check:
                rows[f['key']] = get_row(data_quarter[key][financial], qtr_yr)
                names[f['key']] = f['keyword']

    qtr_yr = remove_quarter(qtr_yr)
    return {'names': names, 'years': qtr_yr, 'rows': rows}


def format_financial(data_annual, data_quarter):

    annual = populate_annuals(data_annual)
    quarter = populate_quarter(data_quarter) if data_quarter else None
    financials = {
        "annual": annual,
        "quarterly": quarter
    }

    return financials


def format_ratios(company):
    fundamentals = company['fundamental_analysis']
    names = populate_name(ratios)
    yr = []
    for f in fundamentals:
        if 'year' in f:
            if f['year'] == 'TTM':
                continue
        else:
            return {'names': {}, 'years': {}, 'rows': {}}
        yr.append(f['year'])

    sliced_yr = yr[:4]
    rows = populate_rows(ratios, fundamentals)

    return {'names': names, 'years': sliced_yr, 'rows': rows}


def delete_company(my_client, company):
    my_db = my_client["filings"]
    collection = my_db["companies"]
    del_query = {"symbol": company}
    collection.delete_one(del_query)


def format_qualitative(qf, d):
    qual = qf['Company Information']
    for q in qual:
        q_key = normalize_keys(q['field'])
        check, financial = match_q_key(q_key)
        if check:
            d[financial['key']] = q['value']
    for info in qf['About']:
        d['description'] = info['value']

    return d


def assign_people(d):
    p_keys = ['secretary', 'chairman', 'ceo']
    people = []
    for key in p_keys:
        if key in d:
            people.append(
                {
                    "position": key.capitalize(),
                    "name": d[key]
                })
    return people


def insert_company(my_client, company, c_ann, c_quart):
    print("Inserting company")
    d = {}
    r = format_ratios(c_ann)
    f = format_financial(c_ann, c_quart)
    d['ratios'] = r
    d['financials'] = f
    year_end = c_ann['income_statement']['yearEnd'] if c_ann['income_statement'] else None
    d['yearEnd'] = year_end[1] if year_end else None
    d['symbol'] = company

    try:
        d = format_qualitative(c_ann['qualitative'], d)
        d['people'] = assign_people(d)
    except KeyError:
        print("key"+"Company Information"+"does not exists")

    my_db = my_client["filings"]
    collection = my_db["companies"]
    collection.insert_one(d)


def update_company(company, my_client):
    data_annual = api_call(company, 'equity')
    data_quarterly = api_call(company, 'quarterly')
    if data_annual is None:
        print("company " + company + "\n", "not found")
        return None
    delete_company(my_client, company)
    insert_company(my_client, company, data_annual, data_quarterly)


def run(arg):
    my_client = pymongo.MongoClient("mongodb://localhost:27017/")
    del arg[0]
    data = get_symbols()
    if len(arg) > 0:
        for company in arg:
            print(company)
            update_company(company, my_client)
    else:
        for company in data:
            print(company)
            update_company(company, my_client)


if __name__ == '__main__':
    run(sys.argv)
